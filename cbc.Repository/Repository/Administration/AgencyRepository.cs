﻿using cbc.Repository.ModelView.Administration;
using cbc.Repository.ModelBinding.Base;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using cbc.Repository.ModelView.Base;
using cbc.Helper.Exceptions;
using cbc.Repository.ModelBinding.Administration;
using cbc.Repository.CustomValidator.Administration;
using System;
using cbc.Database.Models.Administration;
using cbc.Config;
using cbc.Repository.Repository.Base;
using System.Linq.Expressions;

namespace cbc.Repository.Repository.Administration
{
    public class AgencyRepository : BaseRepository<Agency>
    {
        /// <summary>
        /// constructor
        /// </summary>
        public AgencyRepository()
        {
        }

        /// <summary>
        /// Find all register in database with filter
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public async Task<PagedCollectionModelView<AgencyModelView>> FindAll(BaseFilterModelBinding filter)
        {
            var query = source.OrderBy(p => new { p.Region.Name, p.Code });
            Expression<Func<Agency, AgencyModelView>> clause = m => new AgencyModelView
            {
                Id = m.Id,
                Code = m.Code,
                Name = m.Name,
                IsEnable = m.IsEnable,
                CreatedAt = m.CreatedAt,
                RegionName = m.Region.Name
            };
            return await MakePaginationAsync(filter, query, clause);
        }

        /// <summary>
        /// Find by id in database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<AgencyDetailModelView> FindById(int id)
        {
            var agency = await source
               .Select(m => new AgencyDetailModelView
               {
                   Id = m.Id,
                   Name = m.Name,
                   Code = m.Code,
                   IsEnable = m.IsEnable,
                   CreatedAt = m.CreatedAt,
                   RegionName = m.Region.Name,
                   RegionId = m.RegionId
               })
               .Where(p => p.Id == id)
               .SingleOrDefaultAsync();

            if (agency == null)
                throw new NotFoundException(message: string.Format(AppErrors.NotFound, "Agencia", id, "a"));

            return agency;
        }

        /// <summary>
        /// Created register in database
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<bool> Create(AgencyModelBinding model)
        {
            await ValidateModelAsync(new AgencyCustomValidator(context), model);

            var region = await context.Regions.Where(p => p.Id == model.RegionId)
                               .SingleOrDefaultAsync();
            if (region == null)
                throw new NotFoundException(message: string.Format(AppErrors.NotFound, "Región", model.RegionId, "a"));

            source.Add(new Agency
            {
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
                Code = model.Code,
                IsEnable = model.IsEnable,
                Name = model.Name,
                RegionId = model.RegionId ?? 0
            });

            return await context.SaveChangesAsync() > 0;
        }

        /// <summary>
        /// Updated register in database
        /// </summary>
        /// <param name="model"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> Update(AgencyModelBinding model, int id)
        {
            model.Id = id;
            var agency = await source.Where(p => p.Id == model.Id)
                                      .SingleOrDefaultAsync();
            if (agency == null)
                throw new NotFoundException(message: string.Format(AppErrors.NotFound, "Agencia", model.Id, "a"));

            await ValidateModelAsync(new AgencyCustomValidator(context), model);

            var region = await context.Regions.Where(p => p.Id == model.RegionId)
                              .SingleOrDefaultAsync();
            if (region == null)
                throw new NotFoundException(message: string.Format(AppErrors.NotFound, "Región", model.RegionId, "a"));

            agency.UpdatedAt = DateTime.UtcNow;
            agency.Code = model.Code;
            agency.IsEnable = model.IsEnable;
            agency.Name = model.Name;
            agency.RegionId = model.RegionId ?? 0;

            return await context.SaveChangesAsync() > 0;
        }
    }
}
