﻿using cbc.Database;
using cbc.Helper.Exceptions;
using cbc.Helper.Pagination;
using cbc.Repository.ModelBinding.Base;
using cbc.Repository.ModelView.Base;
using FluentValidation;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace cbc.Repository.Repository.Base
{
    public abstract class BaseRepository<T> where T : class
    {
        protected internal ApplicationDbContext context;
        protected internal DbSet<T> source;

        public BaseRepository()
        {
            context = new ApplicationDbContext();
            source = context.Set<T>();
        }

        protected internal async Task<PagedCollectionModelView<TModelView>> MakePaginationAsync<TModelView>(IPagedFilter filter, IQueryable<T> query, Expression<Func<T, TModelView>> clause)
        {
            PaginationHelper.Initialize(filter.Page, filter.PageSize);
            int totalCount = await query.CountAsync();
            var records = await query.Skip(PaginationHelper.SkipValues())
                            .Take(PaginationHelper.PageSize)
                            .Select(clause)
                            .ToListAsync();

            return new PagedCollectionModelView<TModelView>()
            {
                CurrentPage = PaginationHelper.CurrentPage,
                PageSize = PaginationHelper.PageSize,
                Records = records,
                TotalCount = totalCount
            };
        }

        protected internal async Task ValidateModelAsync<TCustomValidator, TModel>(TCustomValidator validator, TModel model, string ruleSet = "*") where TCustomValidator : AbstractValidator<TModel> // IValidator
        {
            ValidationResult results = await validator.ValidateAsync(model, ruleSet: ruleSet);
            bool isValid = results.IsValid;
            IList<ValidationFailure> failures = results.Errors;

            if (!results.IsValid)
            {
                var errors = failures.Select(p => p.ErrorMessage).ToList();
                throw new BusinessException(errors: errors);
            }
        }
    }
}
