﻿namespace cbc.Repository.ModelBinding.Base
{
    public interface IPagedFilter
    {
        int? Page { get; set; }
        int? PageSize { get; set; }
    }

    public interface ISearchFilter
    {
        string Search { get; set; }
    }

    public class PagedFilterModelBinding : IPagedFilter
    {
        public int? Page { get; set; }
        public int? PageSize { get; set; }
    }

    public class SearchFilterModelBinding : ISearchFilter
    {
        public string Search { get; set; }
    }

    public class BaseFilterModelBinding : IPagedFilter, ISearchFilter
    {
        public int? Page { get; set; }
        public int? PageSize { get; set; }
        public string Search { get; set; }
    }
}
