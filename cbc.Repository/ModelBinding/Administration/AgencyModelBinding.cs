﻿using cbc.Database.Models.Administration.Base;

namespace cbc.Repository.ModelBinding.Administration
{
    public class AgencyModelBinding : BaseEntityWithoutTimeStampModel
    {
        public AgencyModelBinding()
        {
        }

        public int? RegionId { get; set; }
    }
}
