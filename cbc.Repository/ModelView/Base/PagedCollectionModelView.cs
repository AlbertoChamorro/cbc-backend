﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace cbc.Repository.ModelView.Base
{
    public class PagedCollectionModelView<T>
    {
        public IEnumerable<T> Records { get; set; }
        // cantidad de registros por pagina
        public int PageSize { get; set; }
        // pagina actual
        public int CurrentPage { get; set; }
        // cantidad de registros devueltos en la pagina
        public int Count => Records?.Count() ?? 0;
        // cantidad de registros en base de datos
        public int TotalCount { get; set; }
        // total de paginas (TotalCount / PageSize) (int)
        public int TotalPages => (int)Math.Ceiling(TotalCount / (decimal)PageSize);
    }
}
