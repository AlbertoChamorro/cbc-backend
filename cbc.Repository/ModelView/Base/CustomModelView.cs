﻿using cbc.Database.Models.Administration.Base;
using Newtonsoft.Json;
using System;

namespace cbc.Repository.ModelView.Base
{
    // model view common
    public abstract class BaseModelView : IBaseEntityIdentity, IBaseCreatedAtDate, IBaseEntityCode, IBaseEntityStatus, IBaseEntityName
    {
        [JsonProperty(Order = 1)]
        public int? Id { get; set; }
        [JsonProperty(Order = 2)]
        public string Code { get; set; }
        [JsonProperty(Order = 3)]
        public bool? IsEnable { get; set; }
        [JsonProperty(Order = 4)]
        public string Name { get; set; }
        [JsonProperty(Order = 5)]
        public DateTime? CreatedAt { get; set; }
    }

    // model view common full
    public abstract class BaseFullModelView : BaseModelView, IBaseEntityDescription
    {
        [JsonProperty(Order = 6)]
        public string Description { get; set; }
    }

    // model view catalogue
    public abstract class BaseCatalogueModelView : BaseEntityCatalogue
    {
    }
}
