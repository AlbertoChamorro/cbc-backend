﻿using cbc.Repository.ModelView.Base;
using Newtonsoft.Json;

namespace cbc.Repository.ModelView.Administration
{
    public class AgencyModelView : BaseModelView
    {
        [JsonProperty(Order = 6)]
        public string RegionName { get; set; }
    }
    
    public class AgencyDetailModelView : AgencyModelView
    {
        [JsonProperty(Order = 6)]
        public int RegionId { get; set; }
    }
}
