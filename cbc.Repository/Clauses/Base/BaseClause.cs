﻿using cbc.Database.Models.Administration.Base;
using System;
using System.Linq.Expressions;

namespace cbc.Repository.Clauses.Base
{
    public class BaseClause
    {
        public static Expression<Func<T, bool>> UniqueName<T>(string comparisonValue) where T : class, IBaseEntityName
        {
            return x => x.Name.ToLower() == comparisonValue.ToLower();
        }

        public static Expression<Func<T, bool>> UniqueCode<T>(string comparisonValue) where T : class, IBaseEntityCode
        {
            return x => x.Code.ToLower() == comparisonValue.ToLower();
        }

        public static Expression<Func<T, bool>> ExcludeCurrentTuple<T>(int? id) where T : class, IBaseEntityIdentity
        {
            return x => x.Id != id;
        }
    }
}
