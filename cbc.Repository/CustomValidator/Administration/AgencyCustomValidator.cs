﻿using cbc.Config;
using cbc.Database;
using cbc.Database.Models.Administration;
using cbc.Repository.Clauses.Base;
using cbc.Repository.CustomValidator.Base;
using cbc.Repository.ModelBinding.Administration;
using FluentValidation;

namespace cbc.Repository.CustomValidator.Administration
{
    public class AgencyCustomValidator : BaseCustomValidator<AgencyModelBinding>
    {
        public AgencyCustomValidator(ApplicationDbContext _context) : base(_context)
        {
            RuleFor(x => x.Id)
                .GreaterThan(0)
                .When(x => x.Id != null)
                .WithMessage(CustomValidatorMessage.REQUIRED);
            RuleFor(x => x.Code)
                .NotEmpty()
                .WithMessage(CustomValidatorMessage.REQUIRED);
            RuleFor(x => x.Name)
                .NotEmpty()
                .WithMessage(CustomValidatorMessage.REQUIRED);
            RuleFor(x => x.Name)
                .MaximumLength(AppEntityConfiguration.MaxLengthName)
                .WithMessage(CustomValidatorMessage.MAX_LENGTH);
            RuleFor(x => x.IsEnable)
                .NotNull()
                .WithMessage(CustomValidatorMessage.REQUIRED).WithName("IsEnable");
            RuleFor(x => x.RegionId)
                .NotEmpty()
                .When(x => x.RegionId == null)
                .WithMessage(CustomValidatorMessage.REQUIRED).WithName("RegionId");
            RuleFor(x => x.RegionId)
                .GreaterThan(0)
                .When(x => x.RegionId != null)
                .WithMessage(CustomValidatorMessage.REQUIRED).WithName("RegionId");

            // Custom validations unique restrictions
            RuleFor(x => x.Name).Must(UniqueName)
                .WithMessage(CustomValidatorMessage.UNIQUE);
            RuleFor(x => x.Code).Must(UniqueCode)
                .WithMessage(CustomValidatorMessage.UNIQUE);
        }

        private bool UniqueName(AgencyModelBinding model, string comparisonValue)
        {
            return ValidateUniqueRestriction(model, comparisonValue, BaseClause.UniqueName<Agency>(comparisonValue));
        }

        private bool UniqueCode(AgencyModelBinding model, string comparisonValue)
        {
            return ValidateUniqueRestriction(model, comparisonValue, BaseClause.UniqueCode<Agency>(comparisonValue));
        }
    }
}
