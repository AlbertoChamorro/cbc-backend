﻿using cbc.Database;
using cbc.Database.Models.Administration.Base;
using cbc.Repository.Clauses.Base;
using FluentValidation;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace cbc.Repository.CustomValidator.Base
{
    public abstract class BaseCustomValidator<T> : AbstractValidator<T>
    {
        protected internal ApplicationDbContext _context;

        public BaseCustomValidator(ApplicationDbContext context)
        {
            _context = context;
        }

        protected internal virtual bool ValidateUniqueRestriction<TSet, TModelBinding>(TModelBinding model, string comparisonValue, Expression<Func<TSet, bool>> expression) where TSet : BaseEntityIdentity where TModelBinding : BaseEntityIdentity
        {
            var query = _context.Set<TSet>().Where(expression);
            if (model.Id != null)
                query = query.Where(BaseClause.ExcludeCurrentTuple<TSet>(model.Id));

            var result = query.SingleOrDefault();
            if (result == null)
                return true;

            return result.Id == model.Id;
        }
    }
}
