﻿namespace cbc.Repository.CustomValidator.Base
{
    public class CustomValidatorMessage
    {
        public const string UNIQUE = "Ya existe un registro con '{PropertyName} {PropertyValue}'";
        public const string REQUIRED = "La propiedad '{PropertyName}' es requerida";
        public const string MAX_LENGTH = "La propiedad '{PropertyName}' no puede tener más de '{MaxLength}' caractéres";
        public const string MIN_LENGTH = "La propiedad '{PropertyName}' no puede tener menos de '{MinLength}' caractéres";
    }
}
