# CBC

Es una compañía multi latina de bebidas con el portafolio más grande de la región y presencia en más de 35 países. Es la compañía de bebidas de las Américas.

## Empezando

Estas instrucciones le permitirán copiar y ejecutar el proyecto en su máquina local para fines de desarrollo y prueba. Consulte la implementación para ver las notas sobre cómo implementar el proyecto en un sistema en vivo.

### Prerrequisitos

Cosas que necesita para instalar el software.

[1. Sistema operativo Windows](https://www.microsoft.com/es-es/software-download/windows8ISO)

[2. Microsoft Visual Studio Express 2017](https://visualstudio.microsoft.com/es/downloads/?rr=https%3A%2F%2Fsocial.msdn.microsoft.com%2FForums%2Fen-US%2F7842f868-175e-4241-8a38-35e9ae83accd%2Fvisual-studio-express-2017%3Fforum%3Dvisualstudiogeneral)

[3. Microsoft Management SQL Server Express 2017](https://www.microsoft.com/en-us/download/details.aspx?id=55994)

[4. Internet Information Services](https://www.microsoft.com/en-us/download/details.aspx?id=48264) (Ojo: si no viene instalado)

### Instalación

Entrar al directorio `/cbc-example` yluego abrá la solución "cbc-backend.sln" en visual studio.

Verifíque su cadena de conexión a su base de datos, está dependerá
de su gestor de Base de datos: LocalDB, SQlite, SQL Server...

```
Web.config -> <configuration> .... </configuration>
```

Una vez lista su conexión ejecute las migraciones y seeders.

```
Update-Database
```

Corra el proyecto y verifique en el browser la web api.

```
http://localhost:9200
```

Adicional a eso puede probar sus servicios web usando alguna herramienta de http como `postman` -> https://www.getpostman.com/

## Corriendo las pruebas

En el proyecto `cbc.Tests` puedes crear tus archivos de pruebas unitarias y de integración, luego ejecutalas y prueba antes de implementarlas en tu proyecto de una sola vez.

### Pruebas de estilo de codificación.

Tener el código bonito es lo mejor que te puede pasar, por tanto guarda cada vez que tengas que aplicar cambios.

```
Aplicar linter por defecto de visual studio 2017 -> ctrl + K + D
```

## Despliegue

Según la configuración del target en el Web.config:
Development, Release, QA, etc.

## Construido con

- [ASP.NET MVC 5 WEB API 2](https://docs.microsoft.com/en-us/aspnet/web-api/overview/getting-started-with-aspnet-web-api/tutorial-your-first-web-api) - El framework web usado
- [NUGET](https://www.nuget.org/profiles/Microsoft) - Gestor de dependencias

## Contribución

Informése sobre el proceso para enviarnos solicitudes de unión vía pull-request.

## Versionamiento

Usamos [GitHub](http://github.com) para el control de versiones.

## Autores

- **Alberto Chamorro** - [GitHub](https://github.com/AlbertoChamorro)

## Licencia

Este proyecto está licenciado bajo la Licencia MIT
