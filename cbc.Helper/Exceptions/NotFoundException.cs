﻿using cbc.Helper.Http;
using System;

namespace cbc.Helper.Exceptions
{
    public class NotFoundException : Exception
    {
        public GenericCustomError Error { get; set; }

        public NotFoundException()
        {
        }

        public NotFoundException(string message) : base(message)
        {
            Error = new GenericCustomError(CustomHttpCode.NOT_FOUND, message);
        }

        public NotFoundException(GenericCustomError error) : base(error.Message)
        {
            Error = error;
        }
    }
}
