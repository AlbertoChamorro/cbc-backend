﻿using cbc.Helper.Http;
using System;
using System.Collections.Generic;

namespace cbc.Helper.Exceptions
{
    public class BusinessException : Exception
    {
        public GenericCustomError Error { get; set; }

        public BusinessException()
        {
        }

        public BusinessException(string message) : base(message)
        {
            Error = new GenericCustomError(CustomHttpCode.BAD_REQUEST, message);
        }

        public BusinessException(string message = "Error de validación", int code = CustomHttpCode.BAD_REQUEST, List<string> errors = null) : base(message)
        {
            Error = new GenericCustomError(code, message, errors);
        }

        public BusinessException(GenericCustomError error) : base(error.Message)
        {
            Error = error;
        }
    }
}
