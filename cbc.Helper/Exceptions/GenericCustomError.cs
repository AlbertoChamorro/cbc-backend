﻿using System.Collections.Generic;

namespace cbc.Helper.Exceptions
{
    public class GenericCustomError
    {
        public int Code { get; set; }
        public string Message { get; set; }
        //[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<string> Errors { get; set; }

        public GenericCustomError(int code, string message, List<string> errors = null)
        {
            Code = code;
            Message = message;
            Errors = errors;
        }

        public GenericCustomError(GenericCustomError customError, List<string> errors = null)
        {
            Code = customError.Code;
            Message = customError.Message;
            Errors = errors;
        }

        public override string ToString()
        {
            return Message;
        }
    }
}
