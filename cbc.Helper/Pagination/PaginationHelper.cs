﻿namespace cbc.Helper.Pagination
{
    public class PaginationHelper
    {
        public const int DEFAULT_PAGE_SISE = 10;
        public const int DEFAULT_PAGE = 1;
        public static int CurrentPage { get; set; }
        public static int PageSize { get; set; }

        public static void Initialize(int? currentPage, int? pageSize)
        {
            CurrentPage = currentPage ?? DEFAULT_PAGE;
            PageSize = pageSize ?? DEFAULT_PAGE_SISE;
        }

        public static int SkipValues()
        {
            return (CurrentPage - 1) * PageSize;
        }
    }
}
