﻿namespace cbc.Helper.Http
{
    public class CustomHttpCode
    {
        public const int INTERNAL_SERVER = 5000;
        public const int BAD_REQUEST = 4000;
        public const int NOT_FOUND = 4004;
        public const int SUCCESS = 2000;
    }
}
