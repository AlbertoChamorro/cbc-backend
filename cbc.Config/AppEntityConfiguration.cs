﻿namespace cbc.Config
{
    public static class AppEntityConfiguration
    {
        public const int MaxLengthName = 75;
        public const int MaxLengthDescription = 256;
    }
}
