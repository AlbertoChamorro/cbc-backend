﻿namespace cbc.Config
{
    public static class AppCode
    {
        public static class Country
        {
            public const string NICARAGUA = "NIC";
        }

        public static class Agency
        {
            public const string MANAGUA = "NC00";
            public const string CHINANDEGA = "NC01";
            public const string LEON = "NC02";
            public const string MASAYA = "NC03";
            
            public const string RIVAS = "NC06";
            public const string SEBACO = "NC07";
            public const string JINOTEGA = "NC09";
            public const string ESTELI = "NC10";
            public const string OCOTAL = "NC11";

            public const string BOACO = "NC12";
            public const string RIO_BLANCO = "NC13";
            public const string JUIGALPA = "NC14";
            public const string SANTO_TOMAS = "NC16";
        }
    }
}
