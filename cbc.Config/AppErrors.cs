﻿using cbc.Helper.Exceptions;

namespace cbc.Config
{
    public class AppErrors
    {
        public static readonly GenericCustomError InternalServer = new GenericCustomError(5000, "En estos momentos el servidor no está operando, intente más tarde.");
        public static readonly GenericCustomError InaccessibleServer = new GenericCustomError(5001, "En estos momentos el servidor de datos está inaccesible, intente más tarde.");

        public static readonly string NotFound = "{0} con identificador {1} no fue encontrad{2}";
    }
}
