﻿using cbc.Database.Models.Administration.Base;

namespace cbc.Database.Models.Administration
{
    public class Employee : BaseEntityIdentity
    {
        public Employee()
        {
        }

        public string Carnet { get; set; }
        public virtual Person Person { get; set; }

        public int WorkPositionId { get; set; }
        public virtual WorkPosition WorkPosition { get; set; }

        public int AgencyId { get; set; }
        public virtual Agency Agency { get; set; }
    }
}
