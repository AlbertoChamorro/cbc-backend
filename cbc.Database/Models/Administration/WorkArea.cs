﻿using cbc.Database.Models.Administration.Base;
using System.Collections.Generic;

namespace cbc.Database.Models.Administration
{
    public class WorkArea : BaseEntityModelFull
    {
        public WorkArea()
        {
            Agencies = new HashSet<Agency>();
            WorkPositions = new HashSet<WorkPosition>();
        }

        public virtual ICollection<Agency> Agencies { get; set; }
        public virtual ICollection<WorkPosition> WorkPositions { get; set; }
    }
}
