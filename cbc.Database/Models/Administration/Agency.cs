﻿using cbc.Database.Models.Administration.Base;
using System.Collections.Generic;

namespace cbc.Database.Models.Administration
{
    public class Agency : BaseEntityModel
    {
        public Agency()
        {
            WorkAreas = new HashSet<WorkArea>();
            Employees = new HashSet<Employee>();
        }

        public int RegionId { get; set; }
        public virtual Region Region { get; set; }

        public virtual ICollection<WorkArea> WorkAreas { get; set; }
        public virtual ICollection<Employee> Employees { get; set; }
    }
}
