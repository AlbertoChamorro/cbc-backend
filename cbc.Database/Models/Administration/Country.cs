﻿using cbc.Database.Models.Administration.Base;
using System.Collections.Generic;

namespace cbc.Database.Models.Administration
{
    public class Country : BaseEntityModel
    {
        public Country()
        {
            Regions = new HashSet<Region>();
        }

        public virtual ICollection<Region> Regions { get; set; }
    }
}
