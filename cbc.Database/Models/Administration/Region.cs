﻿using cbc.Database.Models.Administration.Base;
using System.Collections.Generic;

namespace cbc.Database.Models.Administration
{
    public class Region : BaseEntityModel
    {
        public Region()
        {
            Agencies = new HashSet<Agency>();
        }

        public int CountryId { get; set; }
        public virtual Country Country { get; set; }

        public virtual ICollection<Agency> Agencies { get; set; }
    }
}
