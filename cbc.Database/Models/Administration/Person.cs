﻿using cbc.Database.Models.Administration.Base;

namespace cbc.Database.Models.Administration
{
    public class Person : BaseEntityTimeStampWithStatus
    {
        public Person()
        {
        }

        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string FirstSurname { get; set; }
        public string SecondSurname { get; set; }
        public string Address { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
