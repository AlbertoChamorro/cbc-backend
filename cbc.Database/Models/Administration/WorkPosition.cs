﻿using cbc.Database.Models.Administration.Base;
using System.Collections.Generic;

namespace cbc.Database.Models.Administration
{
    public class WorkPosition : BaseEntityModelFull
    {
        public WorkPosition()
        {
            Employees = new HashSet<Employee>();
        }

        public int WorkAreaId { get; set; }
        public virtual WorkArea WorkArea { get; set; }

        public virtual ICollection<Employee> Employees { get; set; }
    }
}
