﻿namespace cbc.Database.Models.Administration.Base
{
    public interface IBaseEntityName
    {
        string Name { get; set; }
    }

    public interface IBaseEntityDescription
    {
        string Description { get; set; }
    }
}
