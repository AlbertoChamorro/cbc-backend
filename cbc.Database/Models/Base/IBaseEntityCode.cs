﻿namespace cbc.Database.Models.Administration.Base
{
    public interface IBaseEntityCode
    {
        string Code { get; set; }
    }
}
