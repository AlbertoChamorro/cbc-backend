﻿namespace cbc.Database.Models.Administration.Base
{
    public interface IBaseEntityStatus
    {
        bool? IsEnable { get; set; }
    }
}
