﻿using Newtonsoft.Json;
using System;

namespace cbc.Database.Models.Administration.Base
{
    // Entidad base con identity
    public abstract class BaseEntityIdentity : IBaseEntityIdentity
    {
        [JsonIgnore]
        public int? Id { get; set; }
    }

    // Entidad base con identity y estado
    public abstract class BaseEntityStatus : BaseEntityIdentity, IBaseEntityStatus
    {
        public bool? IsEnable { get; set; }
    }

    // Entidad base con identity y fecha de creación
    public abstract class BaseEntityCreatedAt : BaseEntityIdentity, IBaseCreatedAtDate
    {
        public DateTime? CreatedAt { get; set; }
    }

    // Entidad base con identity, estado y fecha de creación
    public abstract class BaseEntityCreatedAtWithStatus : BaseEntityCreatedAt, IBaseEntityStatus
    {
        public bool? IsEnable { get; set; }
    }

    // Entidad base con identity y marca de tiempo
    public abstract class BaseEntityTimeStamp : BaseEntityCreatedAt, IBaseUpdatedAtDate
    {
        public DateTime? UpdatedAt { get; set; }
    }

    // Entidad base con identity, estado y marca de tiempo
    public abstract class BaseEntityTimeStampWithStatus : BaseEntityTimeStamp, IBaseEntityStatus
    {
        public bool? IsEnable { get; set; }
    }

    // Entidad común bd
    public abstract class BaseEntityModel : BaseEntityTimeStamp, IBaseEntityCode, IBaseEntityStatus, IBaseEntityName
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public bool? IsEnable { get; set; }
    }

    // Entidad común full bd
    public abstract class BaseEntityModelFull : BaseEntityModel, IBaseEntityDescription
    {
        public string Description { get; set; }
    }

    // Entidad común for model binder
    public abstract class BaseEntityWithoutTimeStampModel : BaseEntityStatus, IBaseEntityCode, IBaseEntityName
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }

    // Entidad común full for model binder
    public abstract class BaseEntityWithoutTimeStampModelFull : BaseEntityWithoutTimeStampModel, IBaseEntityDescription
    {
        public string Description { get; set; }
    }
}
