﻿using System;

namespace cbc.Database.Models.Administration.Base
{
    public interface IBaseEntityTimeStamp : IBaseCreatedAtDate, IBaseUpdatedAtDate
    {
    }

    public interface IBaseCreatedAtDate
    {
        DateTime? CreatedAt { get; set; }
    }

    public interface IBaseUpdatedAtDate
    {
        DateTime? UpdatedAt { get; set; }
    }
}
