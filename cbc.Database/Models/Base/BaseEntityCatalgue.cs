﻿namespace cbc.Database.Models.Administration.Base
{
    // Entidad base de catalogos
    public abstract class BaseEntityCatalogue : BaseEntityIdentity, IBaseEntityCode, IBaseEntityName
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }

    // Entidad base de catalogos con descripcion
    public abstract class BaseEntityCatalogueWithDescription : BaseEntityCatalogue, IBaseEntityDescription
    {
        public string Description { get; set; }
    }
}
