﻿namespace cbc.Database.Models.Administration.Base
{
    public interface IBaseEntityIdentity
    {
        int? Id { get; set; }
    }
}
