namespace cbc.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixedRelationShipEmployeeAndPerson : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Persons", "Id", "dbo.Employees");
            DropIndex("dbo.Persons", new[] { "Id" });
            DropPrimaryKey("dbo.Employees");
            DropPrimaryKey("dbo.Persons");
            AlterColumn("dbo.Employees", "Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Persons", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Employees", "Id");
            AddPrimaryKey("dbo.Persons", "Id");
            CreateIndex("dbo.Employees", "Id");
            AddForeignKey("dbo.Employees", "Id", "dbo.Persons", "Id");
            DropColumn("dbo.Employees", "PersonId");
            DropColumn("dbo.Persons", "EmployeeId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Persons", "EmployeeId", c => c.Int());
            AddColumn("dbo.Employees", "PersonId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Employees", "Id", "dbo.Persons");
            DropIndex("dbo.Employees", new[] { "Id" });
            DropPrimaryKey("dbo.Persons");
            DropPrimaryKey("dbo.Employees");
            AlterColumn("dbo.Persons", "Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Employees", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Persons", "Id");
            AddPrimaryKey("dbo.Employees", "Id");
            CreateIndex("dbo.Persons", "Id");
            AddForeignKey("dbo.Persons", "Id", "dbo.Employees", "Id");
        }
    }
}
