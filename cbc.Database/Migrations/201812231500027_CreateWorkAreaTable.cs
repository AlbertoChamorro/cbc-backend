namespace cbc.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateWorkAreaTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.WorkAreas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(maxLength: 256),
                        Code = c.String(nullable: false),
                        IsEnable = c.Boolean(nullable: false),
                        Name = c.String(nullable: false, maxLength: 75),
                        UpdatedAt = c.DateTime(),
                        CreatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.WorkAreas");
        }
    }
}
