namespace cbc.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DropColumnDescriptionAtCountry : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Countries", "Description");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Countries", "Description", c => c.String(maxLength: 256));
        }
    }
}
