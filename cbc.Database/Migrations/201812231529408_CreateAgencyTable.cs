namespace cbc.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateAgencyTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Agencies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RegionId = c.Int(nullable: false),
                        Description = c.String(maxLength: 256),
                        Code = c.String(nullable: false),
                        Name = c.String(nullable: false, maxLength: 75),
                        IsEnable = c.Boolean(nullable: false),
                        UpdatedAt = c.DateTime(),
                        CreatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Regions", t => t.RegionId)
                .Index(t => t.RegionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Agencies", "RegionId", "dbo.Regions");
            DropIndex("dbo.Agencies", new[] { "RegionId" });
            DropTable("dbo.Agencies");
        }
    }
}
