namespace cbc.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateRelationShipAgencies_WorkAreas : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Agencies_WorkAreas",
                c => new
                    {
                        AgencyId = c.Int(nullable: false),
                        WorkAreaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.AgencyId, t.WorkAreaId })
                .ForeignKey("dbo.WorkAreas", t => t.AgencyId, cascadeDelete: true)
                .ForeignKey("dbo.Agencies", t => t.WorkAreaId, cascadeDelete: true)
                .Index(t => t.AgencyId)
                .Index(t => t.WorkAreaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Agencies_WorkAreas", "WorkAreaId", "dbo.Agencies");
            DropForeignKey("dbo.Agencies_WorkAreas", "AgencyId", "dbo.WorkAreas");
            DropIndex("dbo.Agencies_WorkAreas", new[] { "WorkAreaId" });
            DropIndex("dbo.Agencies_WorkAreas", new[] { "AgencyId" });
            DropTable("dbo.Agencies_WorkAreas");
        }
    }
}
