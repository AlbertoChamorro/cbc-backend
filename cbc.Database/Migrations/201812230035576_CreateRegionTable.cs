namespace cbc.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateRegionTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Regions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CountryId = c.Int(nullable: false),
                        Code = c.String(nullable: false),
                        IsEnable = c.Boolean(nullable: false),
                        Name = c.String(nullable: false, maxLength: 75),
                        UpdatedAt = c.DateTime(),
                        CreatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.CountryId)
                .Index(t => t.CountryId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Regions", "CountryId", "dbo.Countries");
            DropIndex("dbo.Regions", new[] { "CountryId" });
            DropTable("dbo.Regions");
        }
    }
}
