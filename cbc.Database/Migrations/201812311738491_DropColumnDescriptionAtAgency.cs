namespace cbc.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DropColumnDescriptionAtAgency : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Agencies", "Description");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Agencies", "Description", c => c.String(maxLength: 256));
        }
    }
}
