namespace cbc.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddColumnCodeAtCountry : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Countries", "Code", c => c.String(nullable: false));
            AlterColumn("dbo.Countries", "IsEnable", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Countries", "IsEnable", c => c.Boolean());
            DropColumn("dbo.Countries", "Code");
        }
    }
}
