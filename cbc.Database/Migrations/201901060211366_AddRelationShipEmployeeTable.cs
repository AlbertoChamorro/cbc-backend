namespace cbc.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRelationShipEmployeeTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Employees", "WorkPositionId", c => c.Int(nullable: false));
            AddColumn("dbo.Employees", "AgencyId", c => c.Int(nullable: false));
            CreateIndex("dbo.Employees", "WorkPositionId");
            CreateIndex("dbo.Employees", "AgencyId");
            AddForeignKey("dbo.Employees", "AgencyId", "dbo.Agencies", "Id");
            AddForeignKey("dbo.Employees", "WorkPositionId", "dbo.WorkPositions", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Employees", "WorkPositionId", "dbo.WorkPositions");
            DropForeignKey("dbo.Employees", "AgencyId", "dbo.Agencies");
            DropIndex("dbo.Employees", new[] { "AgencyId" });
            DropIndex("dbo.Employees", new[] { "WorkPositionId" });
            DropColumn("dbo.Employees", "AgencyId");
            DropColumn("dbo.Employees", "WorkPositionId");
        }
    }
}
