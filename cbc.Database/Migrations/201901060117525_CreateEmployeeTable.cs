namespace cbc.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateEmployeeTable : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Persons");
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Carnet = c.String(nullable: false, maxLength: 10),
                        PersonId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Persons", "EmployeeId", c => c.Int());
            AlterColumn("dbo.Persons", "Id", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Persons", "Id");
            CreateIndex("dbo.Persons", "Id");
            AddForeignKey("dbo.Persons", "Id", "dbo.Employees", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Persons", "Id", "dbo.Employees");
            DropIndex("dbo.Persons", new[] { "Id" });
            DropPrimaryKey("dbo.Persons");
            AlterColumn("dbo.Persons", "Id", c => c.Int(nullable: false, identity: true));
            DropColumn("dbo.Persons", "EmployeeId");
            DropTable("dbo.Employees");
            AddPrimaryKey("dbo.Persons", "Id");
        }
    }
}
