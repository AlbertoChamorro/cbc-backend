namespace cbc.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreatePersonTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Persons",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false, maxLength: 75),
                        SecondName = c.String(maxLength: 75),
                        FirstSurname = c.String(nullable: false, maxLength: 75),
                        SecondSurname = c.String(maxLength: 75),
                        Address = c.String(nullable: false, maxLength: 256),
                        IsEnable = c.Boolean(nullable: false),
                        UpdatedAt = c.DateTime(),
                        CreatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Persons");
        }
    }
}
