namespace cbc.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateWorkPositionTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.WorkPositions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        WorkAreaId = c.Int(nullable: false),
                        Description = c.String(maxLength: 256),
                        Code = c.String(nullable: false),
                        Name = c.String(nullable: false, maxLength: 75),
                        IsEnable = c.Boolean(nullable: false),
                        UpdatedAt = c.DateTime(),
                        CreatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.WorkAreas", t => t.WorkAreaId)
                .Index(t => t.WorkAreaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WorkPositions", "WorkAreaId", "dbo.WorkAreas");
            DropIndex("dbo.WorkPositions", new[] { "WorkAreaId" });
            DropTable("dbo.WorkPositions");
        }
    }
}
