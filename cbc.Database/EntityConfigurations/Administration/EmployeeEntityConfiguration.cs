﻿using cbc.Database.Models.Administration;

namespace cbc.Database.EntityConfigurations.Administration
{
    public class EmployeeEntityConfiguration : BaseEntityConfiguration<Employee>
    {
        public EmployeeEntityConfiguration() : base("Employees")
        {
            Property(p => p.Carnet).IsRequired()
                .HasMaxLength(10);

            HasRequired(p => p.Person)
                .WithOptional(p => p.Employee)
                .WillCascadeOnDelete(false);

            HasRequired(p => p.WorkPosition)
                .WithMany(p => p.Employees)
                .HasForeignKey(p => p.WorkPositionId)
                .WillCascadeOnDelete(false);

            HasRequired(p => p.Agency)
                .WithMany(p => p.Employees)
                .HasForeignKey(p => p.AgencyId)
                .WillCascadeOnDelete(false);
        }
    }
}
