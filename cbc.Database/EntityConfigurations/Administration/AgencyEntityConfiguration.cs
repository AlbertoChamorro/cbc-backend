﻿using cbc.Database.Models.Administration;

namespace cbc.Database.EntityConfigurations.Administration
{
    public class AgencyEntityConfiguration : BaseEntityDatabaseConfiguration<Agency>
    {
        public AgencyEntityConfiguration() : base("Agencies")
        {
            HasRequired(p => p.Region)
                .WithMany(r => r.Agencies)
                .HasForeignKey(p => p.RegionId)
                .WillCascadeOnDelete(false);
        }
    }
}
