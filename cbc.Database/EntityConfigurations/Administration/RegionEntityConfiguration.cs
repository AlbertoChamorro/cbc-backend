﻿using cbc.Database.Models.Administration;

namespace cbc.Database.EntityConfigurations.Administration
{
    public class RegionEntityConfiguration : BaseEntityDatabaseConfiguration<Region>
    {
        public RegionEntityConfiguration() : base("Regions")
        {
            HasRequired(p => p.Country)
                .WithMany(c => c.Regions)
                .HasForeignKey(p => p.CountryId)
                .WillCascadeOnDelete(false);
        }
    }
}
