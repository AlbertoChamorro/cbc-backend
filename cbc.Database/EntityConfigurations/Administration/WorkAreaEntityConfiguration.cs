﻿using cbc.Database.Models.Administration;

namespace cbc.Database.EntityConfigurations.Administration
{
    public class WorkAreaEntityConfiguration : BaseEntityDatabaseFullConfiguration<WorkArea>
    {
        public WorkAreaEntityConfiguration() : base("WorkAreas")
        {
            HasMany(p => p.Agencies)
                .WithMany(a => a.WorkAreas)
                .Map(m => {
                    m.MapLeftKey("AgencyId");
                    m.MapRightKey("WorkAreaId");
                    m.ToTable("Agencies_WorkAreas");
                });
        }
    }
}
