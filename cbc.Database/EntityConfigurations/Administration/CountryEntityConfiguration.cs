﻿using cbc.Database.Models.Administration;

namespace cbc.Database.EntityConfigurations.Administration
{
    public class CountryEntityConfiguration : BaseEntityDatabaseConfiguration<Country>
    {
        public CountryEntityConfiguration() : base("Countries")
        {

        }
    }
}
