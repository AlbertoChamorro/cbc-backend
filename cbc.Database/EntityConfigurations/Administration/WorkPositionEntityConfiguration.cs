﻿using cbc.Database.Models.Administration;

namespace cbc.Database.EntityConfigurations.Administration
{
    public class WorkPositionEntityConfiguration : BaseEntityDatabaseFullConfiguration<WorkPosition>
    {
        public WorkPositionEntityConfiguration() : base("WorkPositions")
        {
            HasRequired(p => p.WorkArea)
                .WithMany(p => p.WorkPositions)
                .HasForeignKey(p => p.WorkAreaId)
                .WillCascadeOnDelete(false);
        }
    }
}
