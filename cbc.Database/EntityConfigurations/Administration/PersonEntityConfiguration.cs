﻿using cbc.Config;
using cbc.Database.Models.Administration;

namespace cbc.Database.EntityConfigurations.Administration
{
    public class PersonEntityConfiguration : BaseEntityTimeStampWitStatusConfiguration<Person>
    {
        public PersonEntityConfiguration() : base("Persons")
        {
            Property(p => p.FirstName).IsRequired()
                .HasMaxLength(AppEntityConfiguration.MaxLengthName);

            Property(p => p.SecondName).IsOptional()
                .HasMaxLength(AppEntityConfiguration.MaxLengthName);

            Property(p => p.FirstSurname).IsRequired()
                .HasMaxLength(AppEntityConfiguration.MaxLengthName);

            Property(p => p.SecondSurname).IsOptional()
                .HasMaxLength(AppEntityConfiguration.MaxLengthName);

            Property(p => p.Address).IsRequired()
                .HasMaxLength(AppEntityConfiguration.MaxLengthDescription);
        }
    }
}
