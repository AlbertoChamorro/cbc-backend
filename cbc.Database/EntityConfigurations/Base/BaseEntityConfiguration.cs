﻿using cbc.Config;
using cbc.Database.Models.Administration.Base;
using System.Data.Entity.ModelConfiguration;

namespace cbc.Database.EntityConfigurations.Administration
{
    // Configuración base para toda entidad
    public class BaseEntityConfiguration<TEntity> : EntityTypeConfiguration<TEntity> where TEntity : BaseEntityIdentity
    {
        public BaseEntityConfiguration(string tableName)
        {
            // Property(...).HasColumnOrder(int)
            ToTable(tableName);
            HasKey(p => p.Id);
        }
    }

    // Configuración base con estado
    public class BaseEntityStatusConfiguration<TEntity> : BaseEntityConfiguration<TEntity> where TEntity : BaseEntityStatus
    {
        public BaseEntityStatusConfiguration(string tableName) : base(tableName)
        {
            Property(p => p.IsEnable).IsRequired();
        }
    }

    // Configuración base con fecha de creación
    public class BaseEntityCreatedAtConfiguration<TEntity> : BaseEntityConfiguration<TEntity> where TEntity : BaseEntityCreatedAt
    {
        public BaseEntityCreatedAtConfiguration(string tableName) : base(tableName)
        {
            Property(p => p.CreatedAt).IsOptional();
        }
    }

    // Configuración base con fecha de creación y estado
    public class BaseEntityCreatedAtConfigurationWitStatus<TEntity> : BaseEntityCreatedAtConfiguration<TEntity> where TEntity : BaseEntityCreatedAtWithStatus
    {
        public BaseEntityCreatedAtConfigurationWitStatus(string tableName) : base(tableName)
        {
            Property(p => p.IsEnable).IsRequired();
        }
    }

    // Configuración base con marca de tiempo
    public class BaseEntityTimeStampConfiguration<TEntity> : BaseEntityCreatedAtConfiguration<TEntity> where TEntity : BaseEntityTimeStamp
    {
        public BaseEntityTimeStampConfiguration(string tableName) : base(tableName)
        {
            Property(p => p.UpdatedAt).IsOptional();
        }
    }
    
    // Configuración base con marca de tiempo y estado
    public class BaseEntityTimeStampWitStatusConfiguration<TEntity> : BaseEntityTimeStampConfiguration<TEntity> where TEntity : BaseEntityTimeStampWithStatus
    {
        public BaseEntityTimeStampWitStatusConfiguration(string tableName) : base(tableName)
        {
            Property(p => p.IsEnable).IsRequired();
        }
    }
    
    // Configuración base común para entidades
    public class BaseEntityDatabaseConfiguration<TEntity> : BaseEntityTimeStampConfiguration<TEntity> where TEntity : BaseEntityModel
    {
        public BaseEntityDatabaseConfiguration(string tableName, int? maxLengthName = null) : base(tableName)
        {
            Property(p => p.Code).IsRequired();
            Property(p => p.IsEnable).IsRequired();
            Property(p => p.Name).IsRequired()
                .HasMaxLength(AppEntityConfiguration.MaxLengthName);
        }
    }

    // Configuración base común para entidades full
    public class BaseEntityDatabaseFullConfiguration<TEntity> : BaseEntityDatabaseConfiguration<TEntity> where TEntity : BaseEntityModelFull
    {
        public BaseEntityDatabaseFullConfiguration(string tableName, int? maxLengthName = null, int? maxLengthDescription = null) : base(tableName, maxLengthName)
        {
            Property(p => p.Description).IsOptional()
                .HasMaxLength(AppEntityConfiguration.MaxLengthDescription);
        }
    }
}
