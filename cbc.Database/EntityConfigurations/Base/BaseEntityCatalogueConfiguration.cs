﻿using cbc.Config;
using cbc.Database.Models.Administration.Base;

namespace cbc.Database.EntityConfigurations.Administration
{
    // Configuración base común para catalogos
    public class BaseEntityCatalogueConfiguration<TEntity> : BaseEntityConfiguration<TEntity> where TEntity : BaseEntityCatalogue
    {
        public BaseEntityCatalogueConfiguration(string tableName) : base(tableName)
        {
            Property(p => p.Code).IsRequired();
            Property(p => p.Name).IsRequired()
                .HasMaxLength(AppEntityConfiguration.MaxLengthName);
        }
    }

    // Configuración base común para catalogos full
    public class BaseEntityCatalogueFullConfiguration<TEntity> : BaseEntityCatalogueConfiguration<TEntity> where TEntity : BaseEntityCatalogueWithDescription
    {
        public BaseEntityCatalogueFullConfiguration(string tableName) : base(tableName)
        {
            Property(p => p.Description).IsOptional()
                .HasMaxLength(AppEntityConfiguration.MaxLengthDescription);
        }
    }
}
