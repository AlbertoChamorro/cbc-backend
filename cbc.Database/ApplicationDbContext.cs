﻿using cbc.Database.EntityConfigurations.Administration;
using cbc.Database.Models.Administration;
using System.Data.Entity;

namespace cbc.Database
{

    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext()
            : base("CbcDBCnx")
        {
        }

        public DbSet<Country> Countries { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<WorkArea> WorkAreas { get; set; }
        public DbSet<WorkPosition> WorkPositions { get; set; }
        public DbSet<Agency> Agencies { get; set; }
        public DbSet<Person> Persons { get; set; }
        public DbSet<Employee> Employees { get; set; }

        // open `Package manager console` and run command 
        // `enable-migrations` -> custom migrations
        // its create folder Migrations/Configuration.cs
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // configuration table db fluent API
            modelBuilder.Configurations.Add(new CountryEntityConfiguration());
            modelBuilder.Configurations.Add(new RegionEntityConfiguration());
            modelBuilder.Configurations.Add(new WorkAreaEntityConfiguration());
            modelBuilder.Configurations.Add(new WorkPositionEntityConfiguration());
            modelBuilder.Configurations.Add(new AgencyEntityConfiguration());
            modelBuilder.Configurations.Add(new PersonEntityConfiguration());
            modelBuilder.Configurations.Add(new EmployeeEntityConfiguration());
        }
    }
}
