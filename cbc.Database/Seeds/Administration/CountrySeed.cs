﻿using cbc.Config;
using cbc.Database.Models.Administration;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;

namespace cbc.Database.Seeds.Administration
{
    public partial class AdminSeed
    {
        public static void CountrySeed(ApplicationDbContext context)
        {
            int identity = 0;
            var countries = new List<Country>()
            {
                new Country
                {
                    Id = ++identity,
                    Name = "Nicaragua",
                    Code = AppCode.Country.NICARAGUA,
                    CreatedAt = DateTime.UtcNow,
                    UpdatedAt = DateTime.UtcNow,
                    IsEnable = true,
                }
            };

            countries.ForEach(country => context.Countries.AddOrUpdate(country));
            context.SaveChanges();
        }
    }
}
