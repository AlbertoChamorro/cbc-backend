﻿using cbc.Database.Models.Administration;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;

namespace cbc.Database.Seeds.Administration
{
    public partial class AdminSeed
    {
        public static void WorkAreaSeed(ApplicationDbContext context)
        {
            int identity = 0;
            var areas = new List<WorkArea>()
            {
                new WorkArea
                {
                    Id = ++identity,
                    Name = "Operaciones y Servicios",
                    Description = "Se encarga del sector operativo, es la cara del cliente.",
                    Code = $"A{identity}",
                    CreatedAt = DateTime.UtcNow,
                    UpdatedAt = DateTime.UtcNow,
                    IsEnable = true
                },
            };

            areas.ForEach(area => context.WorkAreas.AddOrUpdate(area));
        }
    }
}
