﻿using cbc.Database.Models.Administration;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;

namespace cbc.Database.Seeds.Administration
{
    public partial class AdminSeed
    {
        public static void WorkPositionSeed(ApplicationDbContext context)
        {
            int identity = 0;
            var positions = new List<WorkPosition>()
            {
                new WorkPosition
                {
                    Id = ++identity,
                    Name = "Administrador",
                    Description = "Encargado de administrar el sistema.",
                    Code = "Admin",
                    IsEnable = true,
                    CreatedAt = DateTime.UtcNow,
                    UpdatedAt = DateTime.UtcNow,
                    WorkAreaId = 1
                },
                new WorkPosition
                {
                    Id = ++identity,
                    Name = "Gerente General de Operaciones y servicios",
                    Description = null,
                    Code = "GOS",
                    IsEnable = true,
                    CreatedAt = DateTime.UtcNow,
                    UpdatedAt = DateTime.UtcNow,
                    WorkAreaId = 1
                },
                new WorkPosition
                {
                    Id = ++identity,
                    Name = "Vendedor de pedidos programados",
                    Description = null,
                    Code = "VPP",
                    IsEnable = true,
                    CreatedAt = DateTime.UtcNow,
                    UpdatedAt = DateTime.UtcNow,
                    WorkAreaId = 1
                },
                new WorkPosition
                {
                    Id = ++identity,
                    Name = "Coordinador de Operaciones y servicios",
                    Description = null,
                    Code = "COS",
                    IsEnable = true,
                    CreatedAt = DateTime.UtcNow,
                    UpdatedAt = DateTime.UtcNow,
                    WorkAreaId = 1
                },
                new WorkPosition
                {
                    Id = ++identity,
                    Name = "Jefe de Operaciones y servicios",
                    Description = null,
                    Code = "JOS",
                    IsEnable = true,
                    CreatedAt = DateTime.UtcNow,
                    UpdatedAt = DateTime.UtcNow,
                    WorkAreaId = 1
                },
                new WorkPosition
                {
                    Id = ++identity,
                    Name = "Controlador de inventario",
                    Description = null,
                    Code = "CI",
                    IsEnable = true,
                    CreatedAt = DateTime.UtcNow,
                    UpdatedAt = DateTime.UtcNow,
                    WorkAreaId = 1
                },
            };

            positions.ForEach(position => context.WorkPositions.AddOrUpdate(position));
        }
    }
}
