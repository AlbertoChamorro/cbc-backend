﻿using cbc.Config;
using cbc.Database.Models.Administration;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;

namespace cbc.Database.Seeds.Administration
{
    public partial class AdminSeed
    {
        public static void RegionSeed(ApplicationDbContext context)
        {
            int identity = 0;
            var nicId = context.Countries
                            .Where(p => p.Code == AppCode.Country.NICARAGUA)
                            .SingleOrDefault().Id ?? 0;
            var regions = new List<Region>()
            {
                new Region
                {
                    Id = ++identity,
                    Name = "Metro",
                    Code = $"R{identity}",
                    CreatedAt = DateTime.UtcNow,
                    UpdatedAt = DateTime.UtcNow,
                    IsEnable = true,
                    CountryId = nicId
                },
                new Region
                {
                    Id = ++identity,
                    Name = "Occidente",
                    Code = $"R{identity}",
                    CreatedAt = DateTime.UtcNow,
                    UpdatedAt = DateTime.UtcNow,
                    IsEnable = true,
                    CountryId = nicId
                },
                new Region
                {
                    Id = ++identity,
                    Name = "Oriente",
                    Code = $"R{identity}",
                    CreatedAt = DateTime.UtcNow,
                    UpdatedAt = DateTime.UtcNow,
                    IsEnable = true,
                    CountryId = nicId
                },
                new Region
                {
                    Id = ++identity,
                    Name = "Norte",
                    Code = $"R{identity}",
                    CreatedAt = DateTime.UtcNow,
                    UpdatedAt = DateTime.UtcNow,
                    IsEnable = true,
                    CountryId = nicId
                },
                new Region
                {
                    Id = ++identity,
                    Name = "Centro",
                    Code = $"R{identity}",
                    CreatedAt = DateTime.UtcNow,
                    UpdatedAt = DateTime.UtcNow,
                    IsEnable = true,
                    CountryId = nicId
                },
            };

            regions.ForEach(region => context.Regions.AddOrUpdate(region));
        }
    }
}
