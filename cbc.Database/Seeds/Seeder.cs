﻿using cbc.Database.Seeds.Administration;

namespace cbc.Database.Seeds
{
    public class Seeder
    {
        public static void Run(ApplicationDbContext context)
        {
            // Execute seed in db
            AdminSeed.CountrySeed(context);
            AdminSeed.RegionSeed(context);
            AdminSeed.WorkAreaSeed(context);
            AdminSeed.WorkPositionSeed(context);

            // save changes in BD
            context.SaveChanges();
        }
    }
}
