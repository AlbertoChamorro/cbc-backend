﻿using System.Net;
using System.Net.Http;

namespace cbc.Api.Common
{
    /// <summary>
    /// BadRequestContentResult class
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BadRequestContentResult<T> : CustomHttpActionResult<T>
    {
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="request"></param>
        /// <param name="content"></param>
        public BadRequestContentResult(HttpRequestMessage request, T content)
            : base(request, content, HttpStatusCode.BadRequest)
        {
        }
    }

    /// <summary>
    /// NotFoundContentResult class
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class NotFoundContentResult<T> : CustomHttpActionResult<T>
    {
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="request"></param>
        /// <param name="content"></param>
        public NotFoundContentResult(HttpRequestMessage request, T content)
            : base(request, content, HttpStatusCode.NotFound)
        {
        }
    }

    /// <summary>
    /// InternalServerErrorContentResult class
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class InternalServerErrorContentResult<T> : CustomHttpActionResult<T>
    {
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="request"></param>
        /// <param name="content"></param>
        public InternalServerErrorContentResult(HttpRequestMessage request, T content)
            : base(request, content, HttpStatusCode.InternalServerError)
        {
        }
    }

    /// <summary>
    /// ServiceUnavailableContentResult class
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ServiceUnavailableContentResult<T> : CustomHttpActionResult<T>
    {
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="request"></param>
        /// <param name="content"></param>
        public ServiceUnavailableContentResult(HttpRequestMessage request, T content)
            : base(request, content, HttpStatusCode.ServiceUnavailable)
        {
        }
    }

    /// <summary>
    /// CreatedContentResult class
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class CreatedContentResult<T> : CustomHttpActionResult<T>
    {
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="request"></param>
        /// <param name="content"></param>
        public CreatedContentResult(HttpRequestMessage request, T content)
            : base(request, content, HttpStatusCode.Created)
        {
        }
    }

    /// <summary>
    /// NoContentResult class
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class NoContentResult<T> : CustomHttpActionResult<T>
    {
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="request"></param>
        /// <param name="content"></param>
        public NoContentResult(HttpRequestMessage request, T content)
            : base(request, content, HttpStatusCode.NoContent)
        {
        }
    }
}