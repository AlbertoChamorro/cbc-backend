﻿using cbc.Config;
using cbc.Helper.Exceptions;
using NLog;
using System;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;

namespace cbc.Api.Common.ActionaFilters
{
    /// <summary>
    /// HandleExceptionAttribute
    /// </summary>
    public class HandleExceptionAttribute : ExceptionFilterAttribute
    {
        /// <summary>
        /// NLog.Logger
        /// </summary>
        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// override OnException -> register all exception in web app
        /// </summary>
        /// <param name="actionExecutedContext"></param>
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            var exception = actionExecutedContext.Exception;
            if (exception is BusinessException)
            {
                var businessException = exception as BusinessException;
                actionExecutedContext.Response = actionExecutedContext.Request
                    .CreateResponse(HttpStatusCode.BadRequest, businessException.Error);

                RegisterLogger(context: actionExecutedContext, ex: exception, isFatalError: false, customError: businessException.Error);
            }
            else if (exception is NotFoundException)
            {
                var notFoundException = exception as NotFoundException;
                actionExecutedContext.Response = actionExecutedContext.Request
                    .CreateResponse(HttpStatusCode.NotFound, notFoundException.Error);

                RegisterLogger(context: actionExecutedContext, ex: exception, isFatalError: false, customError: notFoundException.Error);
            }
            else if (exception is SqlException)
            {
                actionExecutedContext.Response = actionExecutedContext.Request
                    .CreateResponse(HttpStatusCode.ServiceUnavailable, AppErrors.InaccessibleServer);

                RegisterLogger(context: actionExecutedContext, ex: exception, isFatalError: true);
            }
            else if (exception is Exception)
            {
                actionExecutedContext.Response = actionExecutedContext.Request
                    .CreateResponse(HttpStatusCode.InternalServerError, AppErrors.InternalServer);

                RegisterLogger(context: actionExecutedContext, ex: exception, isFatalError: true);
            }

            base.OnException(actionExecutedContext);
        }

        /// <summary>
        /// register logger in destine objective
        /// </summary>
        /// <param name="context"></param>
        /// <param name="ex"></param>
        /// <param name="isFatalError"></param>
        /// <param name="customError"></param>
        private void RegisterLogger(HttpActionExecutedContext context, Exception ex, bool isFatalError, GenericCustomError customError = null)
        {
            var controllerName = context.ActionContext.ControllerContext.ControllerDescriptor.ControllerName;
            var actionName = context.ActionContext.ActionDescriptor.ActionName;
            MappedDiagnosticsLogicalContext.Set("controllerName", controllerName);
            MappedDiagnosticsLogicalContext.Set("actionName", actionName);
            MappedDiagnosticsLogicalContext.Set("httpStatusCode", $"{context.Response.StatusCode} ({(int)context.Response.StatusCode})");
            MappedDiagnosticsLogicalContext.Set("contentBody", GetBodyFromRequest(context));

            if (!isFatalError)
            {
                MappedDiagnosticsLogicalContext.Set("businnessHttpStatusCode", customError.Code);
                if (customError.Errors != null)
                {
                    MappedDiagnosticsLogicalContext.Set("errorList", string.Join(",", customError.Errors));
                }
                logger.Info(ex, ex.Message);
                return;
            }
            logger.Warn(ex, ex.Message);
        }

        /// <summary>
        /// parser body data from request http
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private string GetBodyFromRequest(HttpActionExecutedContext context)
        {
            using (var stream = context.Request.Content.ReadAsStreamAsync().Result)
            {
                if (stream.CanSeek) stream.Position = 0;
                return context.Request.Content.ReadAsStringAsync().Result;
            }
        }
    }
}