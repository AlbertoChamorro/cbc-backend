﻿using System;

namespace cbc.Api.Common
{
    /// <summary>
    /// GenericResponse
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class GenericResponse<T>
    {
        /// <summary></summary>
        public T Data { get; set; }
        /// <summary></summary>
        public DateTime TimeStamp { get; set; }

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="data"></param>
        public GenericResponse(T data)
        {
            Data = data;
            TimeStamp = DateTime.UtcNow;
        }
    }
}