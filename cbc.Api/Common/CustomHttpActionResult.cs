﻿using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace cbc.Api.Common
{
    /// <summary>
    /// CustomHttpActionResult class
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class CustomHttpActionResult<T> : IHttpActionResult
    {
        /// <summary></summary>
        protected readonly T Content;
        /// <summary></summary>
        protected readonly HttpRequestMessage Request;
        /// <summary></summary>
        protected readonly HttpStatusCode Code;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="request"></param>
        /// <param name="content"></param>
        /// <param name="code"></param>
        public CustomHttpActionResult(HttpRequestMessage request, T content, HttpStatusCode code)
        {
            Request = request;
            Content = content;
            Code = code;
        }

        /// <summary>
        /// ExecuteAsync -> HttpResponseMessage
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            return Task.FromResult(ExecuteResult());
        }

        /// <summary>
        /// ExecuteResult -> HttpResponseMessage
        /// </summary>
        /// <returns></returns>
        private HttpResponseMessage ExecuteResult()
        {
            return Request.CreateResponse(Code, Content);
        }
    }
}