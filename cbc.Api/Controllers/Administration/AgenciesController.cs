﻿using cbc.Repository.ModelBinding.Administration;
using cbc.Repository.ModelBinding.Base;
using cbc.Repository.ModelView.Administration;
using cbc.Repository.ModelView.Base;
using cbc.Repository.Repository.Administration;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace cbc.Api.Controllers
{
    /// <summary>
    /// Agency management: (it is part of the administration sub system)
    /// </summary>
    [RoutePrefix("api/agencies")]
    public class AgenciesController : BaseController
    {
        /// <summary>
        /// AgencyRepository instance
        /// </summary>
        private AgencyRepository _agencyRepository;

        /// <summary>
        /// constructor
        /// </summary>
        public AgenciesController()
        {
            _agencyRepository = new AgencyRepository();
        }

        /// GET api/agencies
        /// <summary>
        ///  Do you want the list of agencies? use this resource
        /// </summary>
        /// <returns></returns>
        /// <param name="filter">you can filter based on this model</param>
        [ResponseType(typeof(PagedCollectionModelView<AgencyModelView>))]
        [Route(""), HttpGet]
        public async Task<IHttpActionResult> List([FromUri] BaseFilterModelBinding filter)
        {
            var model = await _agencyRepository.FindAll(filter);
            return ResponseToJson(model);
        }

        /// GET api/agencies/{id}
        /// <summary>
        ///  Do you want the detail an agency? use this resource
        /// </summary>
        /// <returns></returns>
        /// <param name="id">agency identifier</param>
        [ResponseType(typeof(AgencyModelView))]
        [Route("{id}"), HttpGet]
        public async Task<IHttpActionResult> Show(int id)
        {
            var model = await _agencyRepository.FindById(id);
            return ResponseToJson(model);
        }

        /// POST api/agencies
        /// <summary>
        ///  Do you want to create an agency? use this resource
        /// </summary>
        /// <returns></returns>
        /// <param name="agency">agency model bind</param>
        [ResponseType(typeof(bool))]
        [Route(""), HttpPost]
        public async Task<IHttpActionResult> Create([FromBody] AgencyModelBinding agency)
        {
            var model = await _agencyRepository.Create(agency);
            return ResponseToJson(model);
        }

        /// PUT api/agencies/{id}
        /// <summary>
        ///  Do you want to update an agency? use this resource
        /// </summary>
        /// <returns></returns>
        /// <param name="id">agency identifier</param>
        /// <param name="agency">agency model bind</param>
        [ResponseType(typeof(bool))]
        [Route("{id}"), HttpPut]
        public async Task<IHttpActionResult> Edit(int id, [FromBody] AgencyModelBinding agency)
        {
            var model = await _agencyRepository.Update(model: agency, id: id);
            return ResponseToJson(model);
        }
    }
}
