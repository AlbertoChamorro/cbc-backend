﻿using cbc.Api.Common;
using System.Web.Http;
using System.Web.Http.Results;

namespace cbc.Api.Controllers
{
    /// <summary>
    /// BaseController
    /// </summary>
    /// <remarks>All controllers inherit from the base</remarks>
    public abstract class BaseController : ApiController
    {
        /// <summary>
        /// constructor
        /// </summary>
        protected BaseController()
        {
        }

        /// <summary>
        /// ResponseToJson
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="content"></param>
        /// <returns></returns>
        protected internal virtual OkNegotiatedContentResult<GenericResponse<T>> ResponseToJson<T>(T content)
        {
            return new OkNegotiatedContentResult<GenericResponse<T>>(new GenericResponse<T>(content), this);
        }

        /// <summary>
        /// CreatedContentResult
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="content"></param>
        /// <returns></returns>
        protected internal virtual CreatedContentResult<T> Created<T>(T content)
        {
            return new CreatedContentResult<T>(Request, content);
        }

        /// <summary>
        /// NoContentResult
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="content"></param>
        /// <returns></returns>
        protected internal virtual NoContentResult<T> NoContent<T>(T content)
        {
            return new NoContentResult<T>(Request, content);
        }

        /// <summary>
        /// BadRequestContentResult
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="content"></param>
        /// <returns></returns>
        protected internal virtual BadRequestContentResult<T> BadRequest<T>(T content)
        {
            return new BadRequestContentResult<T>(Request, content);
        }

        /// <summary>
        /// NotFoundContentResult
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="content"></param>
        /// <returns></returns>
        protected internal virtual NotFoundContentResult<T> NotFound<T>(T content)
        {
            return new NotFoundContentResult<T>(Request, content);
        }

        /// <summary>
        /// InternalServerErrorContentResult
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="content"></param>
        /// <returns></returns>
        protected internal virtual InternalServerErrorContentResult<T> InternalServerError<T>(T content)
        {
            return new InternalServerErrorContentResult<T>(Request, content);
        }

        /// <summary>
        /// ServiceUnavailableContentResult
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="content"></param>
        /// <returns></returns>
        protected internal virtual ServiceUnavailableContentResult<T> ServiceUnavailable<T>(T content)
        {
            return new ServiceUnavailableContentResult<T>(Request, content);
        }
    }
}